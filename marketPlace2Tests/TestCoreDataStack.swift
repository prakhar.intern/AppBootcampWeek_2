//
//  TestCoreDataStack.swift
//  marketPlace2Tests
//
//  Created by Prakhar Mishra on 09/02/23.
//

import XCTest
@testable import marketPlace2
import CoreData
import SwiftUI

public class TestCoreDataStack:  XCTestCase {
    lazy var persistentC: NSPersistentContainer = {
        let description = NSPersistentStoreDescription()
        description.url = URL(fileURLWithPath: "/dev/null")
        let container = NSPersistentContainer(name: "marketPlace2")
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()
    
    func testFetchProductById() {
        //Given
        
        let context = TestCoreDataStack().persistentC.newBackgroundContext()
        
        expectation(
            forNotification:.NSManagedObjectContextDidSave,
            object: context) { _ in
                return true
            }
        for friendNumber in 0..<10 {
            let newFriend = ItemEnt(context: context)
            newFriend.name = "Friend \(friendNumber)"
            newFriend.price = "0.0"
            newFriend.extra = "No"
        }
        try! context.save()
        waitForExpectations(timeout: 2.0) { error in
            XCTAssertNil(error, "Save did not occur")
        }
        
        //When
        let fetchRequest: NSFetchRequest<ItemEnt> = ItemEnt.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \ItemEnt.name, ascending: true)]
        let result = try? context.fetch(fetchRequest)
        let finalProduct1 = result?.first
        //Then
        XCTAssertEqual(result?.count, 10)
        XCTAssertEqual(finalProduct1?.name, "Friend 0")
    }
}
