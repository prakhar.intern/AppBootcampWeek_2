//
//  ItemEnt+CoreDataProperties.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 07/02/23.
//
//

import Foundation
import CoreData


extension ItemEnt {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemEnt> {
        return NSFetchRequest<ItemEnt>(entityName: "ItemEnt")
    }

    @NSManaged public var name: String?
    @NSManaged public var price: String?
    @NSManaged public var extra: String?
    
}

extension ItemEnt : Identifiable {

}
