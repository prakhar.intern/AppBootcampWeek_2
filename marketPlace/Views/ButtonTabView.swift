//
//  ButtonTabView.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 25/01/23.
//

import Foundation
import SwiftUI
import Combine

struct IndividualButtonView : View {
    @Binding var selectedTag : Int
    @Binding var selectedTagValue : Int
    var body: some View{
        Button(
            action: {
                selectedTag = selectedTagValue
            })
        {
            Image(systemName: "circle.fill")
        }
    }
}

struct DefaultPageView : View {
    @Binding var selectedTag : Int
    @Binding var selectedTagValue : Int
    var body: some View{
        Text("Not found pg \(selectedTagValue)!!")
            .multilineTextAlignment(.center)
            .tabItem {
                IndividualButtonView(selectedTag: $selectedTag, selectedTagValue: .constant(selectedTagValue))
            }
            .tag(selectedTagValue)
    }
}

struct ButtonTabView : View {
    @Binding var selectedTag : Int
    var itemList : FetchedResults<ItemEnt>
    @ObservedObject var viewModel : AppViewModel
    
    let numTabs = 5
    let minDragTranslationForSwipe: CGFloat = 30
    
    var body: some View{
        
        ItemsList(filteredItems : itemList)
            .tabItem {
                IndividualButtonView(selectedTag: $selectedTag, selectedTagValue: .constant(1))
            }
            .tag(1)
            .highPriorityGesture(DragGesture().onEnded({
                self.handleSwipe(translation: $0.translation.width)}))

        ItemsGrid(filteredItems : itemList)
            .tabItem {
                IndividualButtonView(selectedTag: $selectedTag, selectedTagValue: .constant(2))
            }
            .tag(2)
            .highPriorityGesture(DragGesture().onEnded({
                self.handleSwipe(translation: $0.translation.width)}))
        
        DefaultPageView(selectedTag: $selectedTag, selectedTagValue: .constant(3))
            .highPriorityGesture(DragGesture().onEnded({
                self.handleSwipe(translation: $0.translation.width)}))
        
        DefaultPageView(selectedTag: $selectedTag, selectedTagValue: .constant(4))
            .highPriorityGesture(DragGesture().onEnded({
                self.handleSwipe(translation: $0.translation.width)}))
        
        DefaultPageView(selectedTag: $selectedTag, selectedTagValue: .constant(5))
            .highPriorityGesture(DragGesture().onEnded({
                self.handleSwipe(translation: $0.translation.width)}))
    }
    
    public func handleSwipe(translation: CGFloat) {
        if translation > minDragTranslationForSwipe && selectedTag > 1 {
            selectedTag -= 1
            
        } else  if translation < -minDragTranslationForSwipe && selectedTag < numTabs {
            selectedTag += 1
        }
    }
}
