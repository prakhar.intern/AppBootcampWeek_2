//
//  IndividualItemGrid.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 17/01/23.
//

import Foundation
import Swift
import SwiftUI


struct ItemsGrid: View {
    //@Binding var filteredItems : [Item]
    @Environment(\.managedObjectContext) private var viewContext
    var filteredItems : FetchedResults<ItemEnt>
    
    var body: some View {
        NavigationStack {
            ScrollView(.vertical) {
                LazyVGrid(columns: [
                    .init(.adaptive(minimum: 110 , maximum: 110), spacing: 0.0)
                ]) {
                    ForEach(filteredItems) { item in
                        NavigationLink(){
                            Text(item.name ?? "")
                        }
                        label : {
                            VStack(alignment: .leading ) {
                                Image(systemName:"square.fill")
                                    .resizable()
                                    .frame(width:100 , height:100)
                                    .cornerRadius(8.0)
                                    .foregroundColor(.gray.opacity(0.15))
                                Text(item.name ?? "")
                                    .font(.subheadline)
                                    .foregroundColor(Color.black)
                                Text(item.price ?? "")
                                    .font(.subheadline)
                                    .bold()
                                    .foregroundColor(Color.black)
                            }
                            .padding(.top)
                           
                        }
                    }
                }.padding()
            }
            .navigationBarItems(
                leading :
                    Button(action: {
                        print("Edit button pressed...")
                    })
                {
                    Text("Explore")
                        .font(.headline)
                        .foregroundColor(Color.black)
                        .bold()
                },
                trailing:
                    Button(action: {
                        print("Edit button pressed...")
                    })
                {
                    Text("Filter")
                        .foregroundColor(Color("FilterColor"))
                        .bold()
                        .font(.callout)
                }
            )
            .toolbarBackground(
                Color("HeaderBackground"),
                for: .navigationBar)
            .toolbarBackground(.visible, for: .navigationBar)
        }
    }
}

//
//struct SampleView_Previews: PreviewProvider {
//    static var previews: some View {
//        SampleView(viewModel: AppViewModel())
//    }
//}
