//
//  ItemsListView.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 18/01/23.
//

import Foundation
import Swift
import SwiftUI
import CoreData

struct ItemsList: View {
    //@Binding var filteredItems : [Item]
    @Environment(\.managedObjectContext) private var viewContext
    var filteredItems : FetchedResults<ItemEnt>
    
    var body: some View {
        
            NavigationStack{
                List{
                    ForEach(filteredItems) { item in
                        
                        NavigationLink(){
                            Text(item.name ?? "")
                        }
                        label : {
                            IndividualItemList(item : item)
                        }
                    }
                }
                .navigationBarItems(
                    leading :
                        Button(action: {
                            print("Edit button pressed...")
                        })
                    {
                        Text("Explore")
                            .font(.headline)
                            .foregroundColor(Color.black)
                            .bold()
                    },
                    trailing:
                        Button(action: {
                            print("Edit button pressed...")
                        })
                    {
                        Text("Filter")
                            .foregroundColor(Color("FilterColor"))
                            .bold()
                            .font(.callout)
                    }
                )
                .toolbarBackground(
                    Color("HeaderBackground"),
                    for: .navigationBar)
                .toolbarBackground(.visible, for: .navigationBar)
            }
    }
}

struct IndividualItemList: View {
    
    @ObservedObject var item : ItemEnt
    
    var body: some View {
        
        HStack(alignment: .top ) {
            Image(systemName:"square.fill")
                .resizable()
                .frame(width:50, height:50)
                .cornerRadius(16.0)
                .foregroundColor(.gray.opacity(0.15))
            
            VStack(alignment: .leading) {
                Text(item.name ?? "")
                    .font(.callout)
                    .bold()
                
                HStack(alignment: .center) {
                    
                    Text("MRP:")
                        .font(.subheadline)
                        .foregroundColor(Color("MrpTextColor"))
                    Text((item.price ?? ""))
                        .font(.subheadline)
                    Spacer()
                    Text(item.extra ?? "")
                        .font(.caption)
                        .foregroundColor(Color("MrpTextColor"))
                }
            }
        }
    }
}

