//
//  ViewModel.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 23/01/23.
//
import SwiftUI
import Foundation
import Combine
import CoreData

class AppViewModel: ObservableObject, Identifiable {
    @Published var searchText: String
        
    let networkManager = NetworkManager()
    
    init() {
        self.searchText = ""
        storeData()
    }
    
    func storeData(){
        if (networkManager.dataExists() == false){
            networkManager.getData()
        }
        else{
            networkManager.refreshData()
        }
    }
}
