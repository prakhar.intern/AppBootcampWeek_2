//
//  File.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 18/01/23.
import UIKit
import Foundation
import SwiftUI
import Combine

struct SampleView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var selectedTag : Int = 1
    @ObservedObject var viewModel: AppViewModel
    
    init(viewModel: AppViewModel) {
        self.viewModel = viewModel
        UITabBar.appearance().backgroundColor = UIColor(red: 250.0/255, green: 250.0/255, blue: 250.0/255, alpha: 1.0)
        
        let textFieldAppearance = UITextField
                    .appearance(whenContainedInInstancesOf: [UISearchBar.self])
        textFieldAppearance.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    @FetchRequest(sortDescriptors: [SortDescriptor(\.name)])
    var itemList: FetchedResults<ItemEnt>
    
    var body: some View {
        
        TabView(selection: $selectedTag) {
            ButtonTabView(selectedTag: $selectedTag, itemList : itemList , viewModel: viewModel)
        }
        .searchable(text: $viewModel.searchText , placement: .navigationBarDrawer(displayMode: .always) )
        .onChange(of: viewModel.searchText) { value in
            itemList.nsPredicate = viewModel.searchText.isEmpty
            ? nil
            : NSPredicate(format: "name CONTAINS %@ || extra CONTAINS %@", value , value , value)
        }
        .onSubmit(of: .search) {
            itemList.nsPredicate = viewModel.searchText.isEmpty
            ? nil
            : NSPredicate(format: "name CONTAINS %@", viewModel.searchText)
        }
        .scrollContentBackground(.hidden)
        .tint(Color("FilterColor"))
        .refreshable {
            print("Pulled to refresh")
            viewModel.storeData()
        }
    }
}
