//
//  File.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 16/01/23.
//

import Foundation
import SwiftUI
import Swift

struct Item : Identifiable, Codable , Hashable{
     var id    = UUID()
     var name  : String
     var price : String
     var extra : String?
    
    init(id: UUID = UUID(), name  : String, price : String, extra : String?) {
        self.id = id
        self.name = name
        self.price = price
        self.extra = extra
    }
}
