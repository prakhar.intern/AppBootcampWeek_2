//
//  APIModel.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 01/02/23.
//
import Foundation
import CoreData
import SwiftUI
#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

class NetworkManager: NSObject {
    
    struct ItemStoreService : Decodable {
        let status : String
        let error : String?
        let data : Data
        
        struct Data : Decodable {
            let items : [Item]
            
            struct Item : Codable{
                var name : String
                var price : String
                var extra : String?
            }
        }
    }
    
    var session: URLSession?
    
    override init() {
        super.init()
        session = URLSession(configuration: .default, delegate: self, delegateQueue: .main)//.none whyyy????
    }
    
    let context = Persistence.shared.persistentC.viewContext
    
    func dataExists() -> Bool {
        
        let fetchRequest = NSFetchRequest<ItemEnt>(entityName: "ItemEnt")
        do {
            let result = try self.context.fetch(fetchRequest)
            if (!result.isEmpty) {
                print("data exists")
                return true
            }
        } catch {
            print(error)
        }
        return false
    }
    
    func getData() {
                
        var request = URLRequest(url: URL(string: "https://run.mocky.io/v3/b6a30bb0-140f-4966-8608-1dc35fa1fadc")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = session?.dataTask(with: request) { data, response, error in
            //            guard let itemStoreService = data else {
            //                print(String(describing: error))
            //                return
            //            }
            //            print(String(data: itemStoreService, encoding: .utf8)!)
            guard let itemStoreService = try? JSONDecoder().decode(ItemStoreService.self , from: data!) else {return}
            
            for item in itemStoreService.data.items{
                let nEntity = ItemEnt(context: self.context)
                let doublePrice = Double(item.price.substring(from: item.price.index(item.price.startIndex, offsetBy: 2)))
                nEntity.price = String( doublePrice! )
                nEntity.extra = item.extra
                nEntity.name = item.name
                
            }
            do {
                try self.context.save()
            } catch let error {
                print("Error generating friends: \(error)")
            }
            print(itemStoreService)
        }
        task?.resume()
    }
    
    
    func refreshData(){
        
        var request = URLRequest(url: URL(string: "https://run.mocky.io/v3/b6a30bb0-140f-4966-8608-1dc35fa1fadc")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = session?.dataTask(with: request) { data, response, error in
            guard let itemStoreService = try? JSONDecoder().decode(ItemStoreService.self , from: data!) else {return}
            
            for item in itemStoreService.data.items{
                if(self.itemExists(item.name) == false){
                    let nEntity = ItemEnt(context: self.context)
                    let doublePrice = Double(item.price.substring(from: item.price.index(item.price.startIndex, offsetBy: 2)))
                    nEntity.price =  String(doublePrice!)
                    nEntity.extra = item.extra
                    nEntity.name = item.name
                }
                else{
                    let fetchRequest: NSFetchRequest<ItemEnt> = ItemEnt.fetchRequest()
                    fetchRequest.predicate = NSPredicate(format: "name == %@", item.name)
                    let result = try? self.context.fetch(fetchRequest)
                    let finalItem = result?.first
                    let doublePrice = Double(item.price.substring(from: item.price.index(item.price.startIndex, offsetBy: 2)))
                    finalItem?.price = String(doublePrice!)
                    finalItem?.extra = item.extra
                }
            }
            do {
                try self.context.save()
            } catch let error {
                print("Error generating friends: \(error)")
            }
            print("Refreshed the existing data")
        }
        task?.resume()
    }
    
    private func itemExists(_ item: String) -> Bool {
        let context = Persistence.shared.persistentC.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ItemEnt")
        fetchRequest.predicate = NSPredicate(format: "name == %@", item)
        return (((try? context.count(for: fetchRequest)) ?? 0) > 0)
    }
}

extension NetworkManager:  URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
