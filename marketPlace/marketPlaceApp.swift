//
//  marketPlaceApp.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 16/01/23.
//

import SwiftUI

@main
struct marketPlaceApp: App {
    let persistence = Persistence.shared
    
    var body: some Scene {
        WindowGroup {
            SampleView(viewModel: AppViewModel())
                .environment(
                    \.managedObjectContext,
                     persistence.persistentC.viewContext)
        }
    }
}
