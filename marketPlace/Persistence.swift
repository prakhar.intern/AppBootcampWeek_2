//
//  Persistence.swift
//  marketPlace
//
//  Created by Prakhar Mishra on 07/02/23.
//


import CoreData

struct Persistence {
    static let shared = Persistence()
    
    let persistentC: NSPersistentContainer
    
    init(inMemory: Bool = false) {
        persistentC = NSPersistentContainer(name: "marketPlace2")
        if inMemory,
           let storeDesc = persistentC.persistentStoreDescriptions.first {
            storeDesc.url = URL(fileURLWithPath: "/dev/null")
        }
        
        persistentC.loadPersistentStores{ _, error in
            if let error = error as NSError? {
                fatalError("Unable to configure Core Data Store: \(error), \(error.userInfo)")
            }
        }
    }
    
    static var preview: Persistence = {
        let result = Persistence(inMemory: true)
        let viewContext = result.persistentC.viewContext
        for friendNumber in 0..<10 {
            let newFriend = ItemEnt(context: viewContext)
            newFriend.name = "Friend \(friendNumber)"
            newFriend.price = "0.0"
            newFriend.extra = "No"
        }
        do {
            try viewContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return result
    }()
}

