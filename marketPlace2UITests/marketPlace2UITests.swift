//
//  marketPlace2UITests.swift
//  marketPlace2UITests
//
//  Created by Prakhar Mishra on 08/02/23.
//

import XCTest

final class marketPlace2UITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.launch()
    }
    
    func testStyleSwitch() throws {
        
        let tabBar = app.tabBars["Tab Bar"]
        
        let firstButton = tabBar.children(matching: .button).matching(identifier: "Circle").element(boundBy: 0)
        firstButton.tap()
        let secondButton = tabBar.children(matching: .button).matching(identifier: "Circle").element(boundBy: 1)
        secondButton.tap()
        let thirdButton = tabBar.children(matching: .button).matching(identifier: "Circle").element(boundBy: 2)
        thirdButton.tap()
        XCTAssert(app.staticTexts["Not found pg 3!!"].exists)
        let fourthButton = tabBar.children(matching: .button).matching(identifier: "Circle").element(boundBy: 3)
        fourthButton.tap()
        XCTAssert(app.staticTexts["Not found pg 4!!"].exists)
        let fifthButton = tabBar.children(matching: .button).matching(identifier: "Circle").element(boundBy: 4)
        fifthButton.tap()
        XCTAssert(app.staticTexts["Not found pg 5!!"].exists)
    }
    

}
